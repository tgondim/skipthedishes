CREATE DATABASE skipthedishes;

ALTER DATABASE skipthedishes CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'skip'@'localhost' IDENTIFIED BY 'skip123';

GRANT ALL ON skipthedishes.* TO 'skip'@'localhost';

FLUSH PRIVILEGES;