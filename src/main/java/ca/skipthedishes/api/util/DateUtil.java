package ca.skipthedishes.api.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	private static final String COMPLETE_WITH_TIMEZONE = "dd-MM-yyyy'T'HH:mm:ss.SSSZ";
	private static final String COMPLETE_FRIENDLY_HUMAN_READABLE = "dd-MM-yyyy'T'HH:mm:ss.SSSZ";

	public static String formatDate(Date data, String pattern) {
		String retorno = "";
		if (data != null && pattern != null) {
			SimpleDateFormat formatar = new SimpleDateFormat(pattern);
			retorno = formatar.format(data);
		}
		return retorno;
	}
	
	public static String dateToStringCompleteWithTimezone(Date date) {
		return formatDate(date, COMPLETE_WITH_TIMEZONE);
	}

	public static String dateToStringCompleteFriendlyHumanReadable(Date date) {
		return formatDate(date, COMPLETE_FRIENDLY_HUMAN_READABLE);
	}
	
	
} 
