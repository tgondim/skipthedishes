package ca.skipthedishes.api.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.Random;

public class PasswordUtil {

	private static final int PASSWORD_MIN_LENGTH = 8;
    private static final char[] SPECIAL_CHARS = { '!', '@', '_', '$', '&', '*', '.', '/', '#', '?' };
    
	public static char[] cripto(char[] md5) {
        try {
            
            ByteBuffer bb = Charset.forName("UTF-8").encode(CharBuffer.wrap(md5));
            byte[] b = new byte[bb.remaining()];
            bb.get(b);
            
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(b);

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toCharArray();
        } catch (java.security.NoSuchAlgorithmException e) {
//            getLogger().severe(e);
        }
        
        return null;
    }
	
	public static char[] generateRandomPassword() {
        Random secureRandom = new SecureRandom();
        char[] pw = new char[PASSWORD_MIN_LENGTH];
        char[] letters = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789".toCharArray();

        int indexChar = (int)(secureRandom.nextDouble() * SPECIAL_CHARS.length);
        pw[0] = SPECIAL_CHARS[indexChar];

        for (int i = 1; i < PASSWORD_MIN_LENGTH; i++) {
            int index = (int)(secureRandom.nextDouble() * letters.length);
            pw[i] = letters[index];
        }
        
        return pw;
    }
	
	public static boolean isCharEqualTo(char[] array1, char[] array2) {

		for (int i = 0; i < array1.length; i++) {
		    if (array1[i] != array2[i]) { 
		    		return false; 
		    }
		}
		return true;
	}
}
