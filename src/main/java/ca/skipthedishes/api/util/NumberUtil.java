package ca.skipthedishes.api.util;

import java.util.Random;

public class NumberUtil {

	public static long generateRandomStringId() {
		long lowerLimit = 1000000000000000000L;
		long upperLimit = 9223372036854775807L;
		Random r = new Random();
		
		return lowerLimit+((long)(r.nextDouble()*(upperLimit-lowerLimit)));
	}
}
