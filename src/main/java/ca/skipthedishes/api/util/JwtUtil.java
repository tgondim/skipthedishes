package ca.skipthedishes.api.util;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class JwtUtil {
	
	public static String SIGNATURE_KEY = "CBD7C47FDC28E653CF20EE2710336E31D64438CB5C8E24F232F7222E1B9D8FED";
	private static String SIGNATURE_ISSUER = "skipthedishes.ca";
	private static long SIGNATURE_TIMEOUT_IN_MILLIS=600000;
	public static final String ROLES_FIELD = "roles";

	public static String createAuthJws(long id) {
		//The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	 
	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SIGNATURE_KEY);
	    
	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().claim(ROLES_FIELD, "list,with,all,roles,in,a,csv,fashion")//TODO put here the user roles 
	    							    .setId(UUID.randomUUID().toString())
	                                 .setIssuedAt(now)
	                                 .setSubject(String.valueOf(id))
	                                 .setIssuer(SIGNATURE_ISSUER)
	                                 .signWith(signatureAlgorithm, apiKeySecretBytes);
	    
	    long expMillis = nowMillis + SIGNATURE_TIMEOUT_IN_MILLIS;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	 
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();		
	}
	
	public static String getIdFromAuthJwt(String authJwt) {
	    	Claims claims = Jwts.parser()         
	    			.setSigningKey(DatatypeConverter.parseBase64Binary(SIGNATURE_KEY))
	    			.parseClaimsJws(authJwt).getBody();
	    	String id = claims.getSubject();
    	
    	return id;
}
}
