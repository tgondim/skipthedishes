package ca.skipthedishes.api.service;

import ca.skipthedishes.api.controller.CousineController;
import ca.skipthedishes.api.controller.CustomerController;
import ca.skipthedishes.api.controller.OrderController;
import ca.skipthedishes.api.controller.ProductController;
import ca.skipthedishes.api.controller.StoreController;
import spark.Spark;

public class SkipApiService {

	public static void main(String[] args) {
		
		//Customer 
		CustomerController customerController = new CustomerController();
		
		Spark.post("/api/v1/Customer/auth", customerController.auth);
		Spark.post("/api/v1/Customer", customerController.post);
		Spark.get("/api/v1/Customer", customerController.get);
		Spark.get("/api/v1/Customer/:customerId", customerController.getWithId);
		
		//Product
		ProductController productController = new ProductController();
		
		Spark.post("/api/v1/Product", productController.post);
		Spark.get("/api/v1/Product", productController.get);
		Spark.get("/api/v1/Product/:productId", productController.getWithId);
		Spark.get("/api/v1/Product/search/:searchText", productController.searchWithText);
		
		//Store
		StoreController storeController = new StoreController(productController.getProductModel());
		
		Spark.post("/api/v1/Store", storeController.post);
		Spark.get("/api/v1/Store", storeController.get);
		Spark.get("/api/v1/Store/:storeId", storeController.getWithId);
		Spark.get("/api/v1/Store/search/:searchText", storeController.searchWithText);
		Spark.get("/api/v1/Store/:storeId/products", storeController.getProductsWithStoreId);

		//Cousine
		CousineController cousineController = new CousineController(storeController.getStoreModel());
		
		Spark.post("/api/v1/Cousine", cousineController.post);
		Spark.get("/api/v1/Cousine", cousineController.get);
		Spark.get("/api/v1/Cousine/:cousineId", cousineController.getWithId);
		Spark.get("/api/v1/Cousine/search/:searchText", cousineController.searchWithText);
		Spark.get("/api/v1/Cousine/:cousineId/stores", cousineController.getStoresWithCousineId);

		//Order
		OrderController orderController = new OrderController();
		
		Spark.post("/api/v1/Order", orderController.post);
		Spark.get("/api/v1/Order", orderController.get);
//		Spark.get("/api/v1/Order/:orderId", orderController.getWithId);
		Spark.get("/api/v1/Order/customer", orderController.getWithCustomer);

	}
}
