package ca.skipthedishes.api.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewOrderItemPayload implements Validable {

	private int id;
	private int orderId;
	private int productId;
	private double price;
	private int quantity;
	private double total;
	
	@Override
	public boolean isValid() {
		return orderId != 0 && productId != 0  
				&& price >= 0 && quantity > 0 
				&& total >= 0; 
	}
	
}
