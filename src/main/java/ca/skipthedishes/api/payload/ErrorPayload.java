package ca.skipthedishes.api.payload;

import lombok.Data;

@Data
public class ErrorPayload {

	private String error;

	public ErrorPayload(String error) {
		super();
		this.error = error;
	}
	
}
