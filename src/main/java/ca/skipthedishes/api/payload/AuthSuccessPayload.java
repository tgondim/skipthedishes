package ca.skipthedishes.api.payload;

import lombok.Data;

@Data
public class AuthSuccessPayload {
	
	private String authToken;

	public AuthSuccessPayload(String authToken) {
		super();
		this.authToken = authToken;
	}

}
