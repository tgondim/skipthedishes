package ca.skipthedishes.api.payload;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class AuthPayload implements Validable {

	private String email;
	private String password;
	
	@Override
	public boolean isValid() {
		return !email.isEmpty() && !password.isEmpty();
	}
	
}
