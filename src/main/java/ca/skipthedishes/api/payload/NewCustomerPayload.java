package ca.skipthedishes.api.payload;

import java.sql.Timestamp;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class NewCustomerPayload implements Validable {

	private Integer id;
	private String name;
	private String email;
	private String address;
	private Timestamp creation;
	private String password;

	@Override
	public boolean isValid() {
		return !name.isEmpty() && !email.isEmpty() && !address.isEmpty() && !password.isEmpty();
	}
	
}
