package ca.skipthedishes.api.payload;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class NewProductPayload implements Validable {

	private int id;
	private int storeId;
	private String name;
	private String description;
	private Double price;
	
	@Override
	public boolean isValid() {
		return storeId != 0 && !name.isEmpty() && !description.isEmpty() && price >= 0;
	}
}
