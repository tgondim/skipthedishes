package ca.skipthedishes.api.payload;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class NewStorePayload implements Validable {

	private int id;
	private String name;
	private String address;
	private int cousineId;

	@Override
	public boolean isValid() {
		return !name.isEmpty() && !address.isEmpty() && cousineId != 0;
	}
}
