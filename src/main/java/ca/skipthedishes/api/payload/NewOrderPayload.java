package ca.skipthedishes.api.payload;

import java.util.List;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class NewOrderPayload implements Validable {

	private int id;
	private String date;
	private int customerId;
	private String deliveryAddress;
	private String contact;
	private int storeId;
	private List<NewOrderItemPayload> orderItems;
	private double total;
	private String status;
	private String lastUpdate;

	@Override
	public boolean isValid() {
		return customerId != 0 && !deliveryAddress.isEmpty() 
				&& !contact.isEmpty() && storeId != 0 
				&& !orderItems.isEmpty() && total >= 0 
				&& !status.isEmpty();
	}
	
}
