package ca.skipthedishes.api.payload;

import ca.skipthedishes.api.Validable;
import lombok.Data;

@Data
public class NewCousinePayload implements Validable {
	
	private int id;
	private String name;
	
	@Override
	public boolean isValid() {
		return !name.isEmpty();
	}
}
