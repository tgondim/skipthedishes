package ca.skipthedishes.api;

public interface Validable {
	
	boolean isValid();

}
