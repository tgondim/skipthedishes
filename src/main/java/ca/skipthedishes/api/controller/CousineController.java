package ca.skipthedishes.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.skipthedishes.api.HttpResponseStatus;
import ca.skipthedishes.api.model.CousineModel;
import ca.skipthedishes.api.model.StoreModel;
import ca.skipthedishes.api.model.entity.Cousine;
import ca.skipthedishes.api.payload.NewCousinePayload;
import ca.skipthedishes.api.util.JsonUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.utils.StringUtils;

public class CousineController {

	private CousineModel cousineModel;
	private StoreModel storeModel;
	
	public CousineController(StoreModel storeModel) {
		this.cousineModel= new CousineModel();
		this.storeModel = storeModel;
	}
	
	public Route post = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			NewCousinePayload creation = mapper.readValue(req.body(), NewCousinePayload.class);

			if (!creation.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}

			Integer id = cousineModel.createCousine(creation.getName());
			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");

			return id;
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route get = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		Long skip = (StringUtils.isEmpty(req.queryMap("skip").value()) ? null : Long.valueOf(req.queryMap("skip").value()));
		Long limit = (StringUtils.isEmpty(req.queryMap("limit").value()) ? null : Long.valueOf(req.queryMap("limit").value()));
		
		return JsonUtil.dataToJson(cousineModel.getAllCousine(skip, limit));
	};

	public Route getWithId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int cousineId = Integer.valueOf(req.params("cousineId"));
		Cousine cousine = cousineModel.getWithId(cousineId);
		
		return (cousine != null ? JsonUtil.dataToJson(cousine) : "");
	};
	
	public Route searchWithText = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		String searchText = req.params("searchText");
		
		return JsonUtil.dataToJson(cousineModel.searchWithText(searchText));
	};

	public Route getStoresWithCousineId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int cousineId = Integer.valueOf(req.params("cousineId"));
		
		return JsonUtil.dataToJson(storeModel.getWithCousineId(cousineId));
	};

	public CousineModel getCousineModel() {
		return cousineModel;
	}

	public StoreModel getStoreModel() {
		return storeModel;
	}

}
