package ca.skipthedishes.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.skipthedishes.api.HttpResponseStatus;
import ca.skipthedishes.api.model.ProductModel;
import ca.skipthedishes.api.model.entity.Product;
import ca.skipthedishes.api.payload.NewProductPayload;
import ca.skipthedishes.api.util.JsonUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.utils.StringUtils;

public class ProductController {
	
	private ProductModel productModel;
	
	public ProductController() {
		this.productModel = new ProductModel(); 
	}
	
	public Route post = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			NewProductPayload creation = mapper.readValue(req.body(), NewProductPayload.class);

			if (!creation.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}

			Integer id = productModel.createProduct(creation.getStoreId(), creation.getName(), creation.getDescription(), creation.getPrice());
			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");

			return id;
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route get = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		Long skip = (StringUtils.isEmpty(req.queryMap("skip").value()) ? null : Long.valueOf(req.queryMap("skip").value()));
		Long limit = (StringUtils.isEmpty(req.queryMap("limit").value()) ? null : Long.valueOf(req.queryMap("limit").value()));
		
		return JsonUtil.dataToJson(productModel.getAllProducts(skip, limit));
	};

	public Route getWithId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int productId = Integer.valueOf(req.params("productId"));
		Product product = productModel.getWithId(productId);
				
		return (product != null ? JsonUtil.dataToJson(product) : "");
	};
	
	public Route searchWithText = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		String searchText = req.params("searchText");
		
		return JsonUtil.dataToJson(productModel.searchWithText(searchText));
	};
	
	public ProductModel getProductModel() {
		return productModel;
	}
}
