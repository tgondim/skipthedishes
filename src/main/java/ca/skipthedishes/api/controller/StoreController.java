package ca.skipthedishes.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.skipthedishes.api.HttpResponseStatus;
import ca.skipthedishes.api.model.ProductModel;
import ca.skipthedishes.api.model.StoreModel;
import ca.skipthedishes.api.model.entity.Store;
import ca.skipthedishes.api.payload.NewStorePayload;
import ca.skipthedishes.api.util.JsonUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.utils.StringUtils;

public class StoreController {

	private StoreModel storeModel;
	private ProductModel productModel;
	
	public StoreController(ProductModel productModel) {
		this.storeModel = new StoreModel();
		this.productModel = productModel;
	}
	
	public Route post = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			NewStorePayload creation = mapper.readValue(req.body(), NewStorePayload.class);

			if (!creation.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}

			Integer id = storeModel.createStore(creation.getName(), creation.getAddress(), creation.getCousineId());
			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");

			return id;
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route get = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		Long skip = (StringUtils.isEmpty(req.queryMap("skip").value()) ? null : Long.valueOf(req.queryMap("skip").value()));
		Long limit = (StringUtils.isEmpty(req.queryMap("limit").value()) ? null : Long.valueOf(req.queryMap("limit").value()));
		
		return JsonUtil.dataToJson(storeModel.getAllStores(skip, limit));
	};

	public Route getWithId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int storeId = Integer.valueOf(req.params("storeId"));
		Store store = storeModel.getWithId(storeId);
		
		return (store != null ? JsonUtil.dataToJson(store) : "");
	};
	
	public Route searchWithText = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		String searchText = req.params("searchText");
		
		return JsonUtil.dataToJson(storeModel.searchWithText(searchText));
	};

	public Route getProductsWithStoreId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int storeId = Integer.valueOf(req.params("storeId"));
		
		return JsonUtil.dataToJson(productModel.getWithStoreId(storeId));
	};
	
	public StoreModel getStoreModel() {
		return storeModel;
	}
	
	public ProductModel getProductModel() {
		return productModel;
	}

}
