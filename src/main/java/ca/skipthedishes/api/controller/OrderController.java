package ca.skipthedishes.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.skipthedishes.api.HttpResponseStatus;
import ca.skipthedishes.api.model.OrderModel;
import ca.skipthedishes.api.model.entity.Order;
import ca.skipthedishes.api.payload.NewOrderPayload;
import ca.skipthedishes.api.util.JsonUtil;
import ca.skipthedishes.api.util.JwtUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.utils.StringUtils;

public class OrderController {

	private OrderModel orderModel;
//	private CustomerModel customerModel;

	public OrderController() {
		 this.orderModel = new OrderModel();
	}
	
	public Route post = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			NewOrderPayload creation = mapper.readValue(req.body(), NewOrderPayload.class);

			if (!creation.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}

			Integer id = orderModel.createOrder(creation.getCustomerId(), creation.getDeliveryAddress(), creation.getContact(), 
												creation.getStoreId(), creation.getOrderItems(), creation.getTotal(), creation.getStatus());
			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");

			return id;
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route get = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		Long skip = (StringUtils.isEmpty(req.queryMap("skip").value()) ? null : Long.valueOf(req.queryMap("skip").value()));
		Long limit = (StringUtils.isEmpty(req.queryMap("limit").value()) ? null : Long.valueOf(req.queryMap("limit").value()));
		
		return JsonUtil.dataToJson(orderModel.getAllOrders(skip, limit));
	};

	public Route getWithId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int orderId = Integer.valueOf(req.params("orderId"));
		Order order = orderModel.getWithId(orderId);
		
		return (order != null? JsonUtil.dataToJson(order) : "") ;
	};

	public Route getWithCustomer = (Request req, Response res) -> {
		
		String authToken = null;
		String authorizationHeader = req.headers("Authorization");
		
		if (!StringUtils.isEmpty(authorizationHeader)) {
			authToken = authorizationHeader.substring("Bearer ".length());
		}

		if (authToken == null) {
			res.status(HttpResponseStatus.HTTP_NOT_AUTHORIZED);
			return "";
		}
		
		String customerId = JwtUtil.getIdFromAuthJwt(authToken);
		
		if (StringUtils.isEmpty(customerId)) {
			res.status(HttpResponseStatus.HTTP_NOT_AUTHORIZED);
			return "";
		}

		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		return JsonUtil.dataToJson(orderModel.getWithCustomerId(Integer.valueOf(customerId)));
	};

	public OrderModel getOrderModel() {
		return orderModel;
	}
	
//	public CustomerModel getCustomerModel() {
//		return customerModel;
//	}
	
}
