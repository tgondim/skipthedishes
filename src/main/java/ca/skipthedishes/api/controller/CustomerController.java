package ca.skipthedishes.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.skipthedishes.api.HttpResponseStatus;
import ca.skipthedishes.api.model.CustomerModel;
import ca.skipthedishes.api.model.entity.Customer;
import ca.skipthedishes.api.payload.AuthPayload;
import ca.skipthedishes.api.payload.AuthSuccessPayload;
import ca.skipthedishes.api.payload.ErrorPayload;
import ca.skipthedishes.api.payload.NewCustomerPayload;
import ca.skipthedishes.api.util.JsonUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.utils.StringUtils;

public class CustomerController {
	
	private CustomerModel customerModel;
	
	public CustomerController() {
		super();
		this.customerModel = new CustomerModel();
	}
	
	public Route auth = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			AuthPayload auth = mapper.readValue(req.body(), AuthPayload.class);
			
			if (!auth.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}
			
			String authToken = customerModel.loginCustomer(auth.getEmail(), auth.getPassword());
			
			if (authToken == null) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				ErrorPayload error = new ErrorPayload("User and Password not found.");
				return JsonUtil.dataToJson(error);
			}

			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");
			
			return JsonUtil.dataToJson(new AuthSuccessPayload(authToken));
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route post = (Request req, Response res) -> {
		try {
			ObjectMapper mapper = new ObjectMapper();
			NewCustomerPayload creation = mapper.readValue(req.body(), NewCustomerPayload.class);
			
			if (!creation.isValid()) {
				res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
				return "";
			}
			
			Integer id = customerModel.createCustomer(creation.getName(), creation.getEmail(), creation.getAddress(), creation.getPassword());
			res.status(HttpResponseStatus.SUCCESS);
			res.type("application/json");
			
			return id;
		} catch (JsonParseException e) {
			res.status(HttpResponseStatus.HTTP_BAD_REQUEST);
			return "";
		}
	};
	
	public Route get = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		Long skip = (StringUtils.isEmpty(req.queryMap("skip").value()) ? null : Long.valueOf(req.queryMap("skip").value()));
		Long limit = (StringUtils.isEmpty(req.queryMap("limit").value()) ? null : Long.valueOf(req.queryMap("limit").value()));
		
		return JsonUtil.dataToJson(customerModel.getAllCustomers(skip, limit));
	};

	public Route getWithId = (Request req, Response res) -> {
		res.status(HttpResponseStatus.SUCCESS);
		res.type("application/json");
		
		int customerId = Integer.valueOf(req.params("customerId"));
		Customer customer = customerModel.getWithId(customerId);
		
		return (customer != null ? JsonUtil.dataToJson(customer) : "");
	};

	public CustomerModel getCustomerModel() {
		return customerModel;
	}

}
