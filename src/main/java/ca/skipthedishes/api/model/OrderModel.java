package ca.skipthedishes.api.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.skipthedishes.api.model.entity.Order;
import ca.skipthedishes.api.model.entity.OrderItem;
import ca.skipthedishes.api.payload.NewOrderItemPayload;

public class OrderModel {

	private int nextId = 1;
	private Map<Integer, Order> orderRepo = new HashMap<Integer, Order>();
	
	public int createOrder(int customerId, String deliveryAddress, String contact, 
			int storeId, List<NewOrderItemPayload> orderItems, double total, String status) {
		int id = nextId++;
		Order order = new Order();
		//TODO need to check if the store and customer exists
		order.setId(id);
		order.setCustomerId(customerId);
		order.setDeliveryAddress(deliveryAddress);
		order.setContact(contact);
		order.setStoreId(storeId);
		order.setOrderItems(new ArrayList<OrderItem>());
		
		orderItems.stream().forEach(x -> {
			OrderItem orderItem = new OrderItem();
			orderItem.setId(x.getId());
			orderItem.setOrderId(x.getOrderId());
			orderItem.setPrice(x.getPrice());
			orderItem.setProductId(x.getProductId());
			orderItem.setQuantity(x.getQuantity());
			orderItem.setTotal(x.getTotal());
			
			order.getOrderItems().add(orderItem);
		});
		
		order.setTotal(total);
		orderRepo.put(id, order);
		
		return id;
	}
	
	public List<Order> getAllOrders(Long skip, Long limit) {
		Stream<Integer> orderStream = orderRepo.keySet().stream();
		orderStream = (skip != null && skip > 0 ? orderStream.skip(skip): orderStream);
		orderStream = (limit != null && limit > 0 ? orderStream.limit(limit): orderStream);
		
		return orderStream.sorted().map(x -> orderRepo.get(x)).collect(Collectors.toList());
	}
	
	public Order getWithId(int orderId) {
		return orderRepo.values().stream().filter(x -> orderId == x.getId()).findAny().orElse(null);
	}
	
	public List<Order> getWithCustomerId(int customerId) {
		return orderRepo.values().stream().filter(x -> customerId == x.getCustomerId()).collect(Collectors.toList());
	}
}
