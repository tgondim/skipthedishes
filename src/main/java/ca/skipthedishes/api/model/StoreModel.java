package ca.skipthedishes.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.skipthedishes.api.model.entity.Product;
import ca.skipthedishes.api.model.entity.Store;

public class StoreModel {
	
	private int nextId = 1;
	private Map<Integer, Store> storeRepo = new HashMap<Integer, Store>();
	
	public int createStore(String name, String address, int cousineId) {
		int id = nextId++;
		Store store = new Store();
		//TODO need to check if the cousine exists
		store.setId(id);
		store.setName(name);
		store.setAddress(address);
		store.setCousineId(cousineId);
		storeRepo.put(id, store);
		
		return id;
	}
	
	public List<Store> getAllStores(Long skip, Long limit) {
		Stream<Integer> storeStream = storeRepo.keySet().stream();
		storeStream = (skip != null && skip > 0 ? storeStream.skip(skip): storeStream);
		storeStream = (limit != null && limit > 0 ? storeStream.limit(limit): storeStream);
		
		return storeStream.sorted().map(x -> storeRepo.get(x)).collect(Collectors.toList());
	}
	
	public Store getWithId(int storeId) {
		return storeRepo.values().stream().filter(x -> storeId == x.getId()).findAny().orElse(null);
	}

	public List<Store> getWithCousineId(int cousineId) {
		return storeRepo.values().stream().filter(x -> cousineId == x.getCousineId()).collect(Collectors.toList());
	}
	
	public List<Store> searchWithText(String searchText) {
		return storeRepo.values().stream().filter(x -> x.getName().toLowerCase().contains(searchText.toLowerCase())).collect(Collectors.toList());
	}
	
}
