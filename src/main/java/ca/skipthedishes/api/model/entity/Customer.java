package ca.skipthedishes.api.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="customer", schema = "skipthedishes")
public class Customer extends AbstractEntity {
	
	@Id
	private long id;
	
	private String name;
	
	private String email;
	
	private String address;
	
	private String creation;
	
	private String password;
	
}
