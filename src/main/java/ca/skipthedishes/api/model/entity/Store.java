package ca.skipthedishes.api.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="store", schema = "skipthedishes")
public class Store extends AbstractEntity {

	@Id
	private long id;
	
	private String name;
	
	private String address;
	
	private int cousineId;
	
}
