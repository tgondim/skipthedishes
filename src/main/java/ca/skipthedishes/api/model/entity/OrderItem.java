package ca.skipthedishes.api.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name="order_item", schema = "skipthedishes")
public class OrderItem extends AbstractEntity {

	@Id
	private long id;
	
	private int orderId;
	
	private int productId;
	
	@Transient
	private Product product;
	
	@ManyToOne
    @JoinColumn(name="order_id", nullable=false)
	private Order order;
	
	private double price;
	
	private int quantity;
	
	private double total;
	
	public Product getProduct() {
		return this.product;
	}
	
}
