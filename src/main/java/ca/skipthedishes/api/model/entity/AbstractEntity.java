package ca.skipthedishes.api.model.entity;

public abstract class AbstractEntity {

	protected AbstractEntity() {
   		super();
   }

   public abstract long getId();

   public abstract void setId(long id);
}
