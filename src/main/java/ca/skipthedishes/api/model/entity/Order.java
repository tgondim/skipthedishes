package ca.skipthedishes.api.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="order", schema = "skipthedishes")
public class Order extends AbstractEntity {
	
	@Id
	private long id;
	
	private String date;
	
	private int customerId;
	
	private String deliveryAddress;
	
	private String contact;
	
	private int storeId;
	
	@OneToMany(mappedBy = "order")
	private List<OrderItem> orderItems;
	
	private double total;
	
	private String lastUpdate;
	
}
