package ca.skipthedishes.api.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="cousine", schema = "skipthedishes")
public class Cousine extends AbstractEntity {

	@Id
	private long id;
	private String name;
	
}
