package ca.skipthedishes.api.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="product", schema = "skipthedishes")
public class Product extends AbstractEntity {

	@Id
	private long id;
	
	private int storeId;
	
	private String name;
	
	private String description;
	
	private double price;

}
