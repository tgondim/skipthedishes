package ca.skipthedishes.api.model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.skipthedishes.api.model.entity.Customer;
import ca.skipthedishes.api.persistence.repository.CustomerSQLRepository;
import ca.skipthedishes.api.util.DateUtil;
import ca.skipthedishes.api.util.JwtUtil;
import ca.skipthedishes.api.util.PasswordUtil;

public class CustomerModel {
	
	private int nextId = 1;
	private Map<Integer, Customer> customerRepo = new HashMap<Integer, Customer>();
//	private CustomerSQLRepository customerRepository = new CustomerSQLRepository();
	
	public String loginCustomer(String email, String password) {
		
		Customer customer = getWithEmail(email);
		
		if (customer == null
				|| !PasswordUtil.isCharEqualTo(PasswordUtil.cripto(password.toCharArray()), 
						customer.getPassword().toCharArray())) {
			return null;
		}
		
		String jwtToken = JwtUtil.createAuthJws(customer.getId());
		
		return jwtToken;
	}
	
	public int createCustomer(String name, String email, String address, String password) {
		int id = nextId++;
		Customer customer = new Customer();
		customer.setId(id);
		customer.setName(name);
		customer.setEmail(email);
		customer.setAddress(address);
		customer.setPassword(String.valueOf(PasswordUtil.cripto(password.toCharArray())));
		customer.setCreation(DateUtil.dateToStringCompleteWithTimezone(new Date()));
		customerRepo.put(id, customer);
//		customerRepository.saveOrUpdateObject(customer);
		
		return id;
	}
	
	public List<Customer> getAllCustomers(Long skip, Long limit) {
		Stream<Integer> customerStream = customerRepo.keySet().stream();
		customerStream = (skip != null && skip > 0 ? customerStream.skip(skip): customerStream);
		customerStream = (limit != null && limit > 0 ? customerStream.limit(limit): customerStream);
		
		return customerStream.sorted().map(x -> customerRepo.get(x)).collect(Collectors.toList());
	}
	
	public Customer getWithId(int customerId) {
		return customerRepo.values().stream().filter(x -> customerId == x.getId()).findAny().orElse(null);
	}
	
	public Customer getWithEmail(String email) {
		return customerRepo.values().stream().filter(x -> email.equals(x.getEmail())).findAny().orElse(null);
	}
	
}
