package ca.skipthedishes.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.skipthedishes.api.model.entity.Cousine;
import ca.skipthedishes.api.model.entity.Product;
import ca.skipthedishes.api.model.entity.Store;

public class ProductModel {

	private int nextId = 1;
	private Map<Integer, Product> productRepo = new HashMap<Integer, Product>();
	
	public int createProduct(int storeId, String name, String description, double price) {
		int id = nextId++;
		Product product = new Product();
		//TODO need to check if the store exists
		product.setId(id);
		product.setStoreId(storeId);
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		productRepo.put(id, product);
		
		return id;
	}
	
	public List<Product> getAllProducts(Long skip, Long limit) {
		Stream<Integer> productStream = productRepo.keySet().stream();
		productStream = (skip != null && skip > 0 ? productStream.skip(skip): productStream);
		productStream = (limit != null && limit > 0 ? productStream.limit(limit): productStream);
		
		return productStream.sorted().map(x -> productRepo.get(x)).collect(Collectors.toList());
	}
	
	public Product getWithId(int productId) {
		return productRepo.values().stream().filter(x -> productId == x.getId()).findAny().orElse(null);
	}
	
	public List<Product> getWithStoreId(int storeId) {
		return productRepo.values().stream().filter(x -> storeId == x.getStoreId()).collect(Collectors.toList());
	}
	
	public List<Product> searchWithText(String searchText) {
		return productRepo.values().stream().filter((x) -> {
			return x.getName().toLowerCase().contains(searchText.toLowerCase()) 
					|| x.getDescription().toLowerCase().contains(searchText.toLowerCase());
		}).collect(Collectors.toList());
	}
}
