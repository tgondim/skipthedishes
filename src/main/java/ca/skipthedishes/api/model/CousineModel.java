package ca.skipthedishes.api.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.skipthedishes.api.model.entity.Cousine;

public class CousineModel {

	private int nextId = 1;
	private Map<Integer, Cousine> cousineRepo = new HashMap<Integer, Cousine>();
	
	public int createCousine(String name) {
		int id = nextId++;
		Cousine cousine = new Cousine();
		cousine.setId(id);
		cousine.setName(name);
		cousineRepo.put(id, cousine);
		
		return id;
	}
	
	public List<Cousine> getAllCousine(Long skip, Long limit) {
		Stream<Integer> cousineStream = cousineRepo.keySet().stream();
		cousineStream = (skip != null && skip > 0 ? cousineStream.skip(skip): cousineStream);
		cousineStream = (limit != null && limit > 0 ? cousineStream.limit(limit): cousineStream);
		
		return cousineStream.sorted().map(x -> cousineRepo.get(x)).collect(Collectors.toList());
	}
	
	public Cousine getWithId(int cousineId) {
		return cousineRepo.values().stream().filter(x -> cousineId == x.getId()).findAny().orElse(null);
	}
	
	public List<Cousine> searchWithText(String searchText) {
		return cousineRepo.values().stream().filter(x -> x.getName().toLowerCase().contains(searchText.toLowerCase())).collect(Collectors.toList());
	}
	
}
