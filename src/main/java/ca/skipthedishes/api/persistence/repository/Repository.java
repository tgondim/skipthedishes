package ca.skipthedishes.api.persistence.repository;

import java.util.List;

public interface Repository<T> {
	
//	public List<T> getAllObjects(int firstRow, int records);

	public T saveOrUpdateObject(T object);

//	public T getObject(long id);
	
//	public boolean deleteObject(long id);
	
	public boolean deleteObject(T object);
}
