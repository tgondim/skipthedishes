package ca.skipthedishes.api.persistence.repository;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import ca.skipthedishes.api.model.entity.AbstractEntity;
import ca.skipthedishes.api.persistence.HibernateUtil;
import ca.skipthedishes.api.util.NumberUtil;

public class SqlRepository<T> implements Repository<T> {

//	private MyLogger logger;

	@Override
	public T saveOrUpdateObject(T object) {
		Session session = null;
	    Transaction transaction = null;
	    try {
	        session = HibernateUtil.getSessionFactory().getCurrentSession();
	        transaction = session.beginTransaction();
	        if (((AbstractEntity)object).getId() != 0) {
	        		((AbstractEntity)object).setId(getNewId());
	        }
	        session.saveOrUpdate(object);
	        session.flush();
	        transaction.commit();
	    } catch (JDBCException jde) {
//	        getLogger().severe(jde);
	        transaction.rollback();
	        throw new RuntimeException(jde);
	    } finally {
	        if (session.isOpen()) {
	            session.close();
	        }
	    }
	    
	    return object;
	}
	
	@Override
	public boolean deleteObject(T object) {
		Session session = null;
	    Transaction transaction = null;

	    if (object == null) {
	    		return false;
	    }
	    
	    try {
	        session = HibernateUtil.getSessionFactory().getCurrentSession();
	        transaction = session.beginTransaction();
	        session.delete(object);
	        session.flush();
	        transaction.commit();
	    } catch (JDBCException jde) {
//	        getLogger().severe(jde);
	        transaction.rollback();
	        throw new RuntimeException(jde);
	    } finally {
	        if (session.isOpen()) {
	            session.close();
	        }
	    }
		
		return true;
	}
	
	public List<T> executeTransactionNamedQueryResultList(String queryName, Class<T> classType, Map<String, Object> parameters) {
		Session session = null;
	    Transaction transaction = null;
	    List<T> entityList = null;
	    
	    try {
	        session = HibernateUtil.getSessionFactory().getCurrentSession();
	        transaction = session.beginTransaction();

			Query<T> query = session.createNamedQuery(queryName, classType);
			
			if (parameters != null) {
				Iterator<Map.Entry<String, Object>> parametersIterator = parameters.entrySet().iterator();
				Map.Entry<String, Object> entry;
				
				while (parametersIterator.hasNext()) {
					entry = parametersIterator.next();
					if ("firstRow".equals(entry.getKey())) {
						if (!new Integer(0).equals(entry.getValue())) {
							query.setFirstResult((Integer)entry.getValue());
						}
					} else if ("records".equals(entry.getKey())) {
						if (!new Integer(0).equals(entry.getValue())) {
							query.setMaxResults((Integer)entry.getValue());
						}
					} else {
						query.setParameter((String)entry.getKey(), entry.getValue());
					}
				}
			}
			
			entityList = query.list();
	        
	        session.flush();
	        transaction.commit();
	    } catch (JDBCException jde) {
//	        getLogger().severe(jde);
	        transaction.rollback();
	        throw new RuntimeException(jde);
	    } finally {
	        if (session.isOpen()) {
	            session.close();
	        }
	    }
	    
		return entityList;
	}
	
	public T executeTransactionNamedQuerySingleResult(String queryName, Class<T> classType, Map<String, Object> parameters) {
		Session session = null;
		Transaction transaction = null;
		T entity = null;
		
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			
			Query<T> query = session.createNamedQuery(queryName, classType);
			
			if (parameters != null) {
				Iterator<Map.Entry<String, Object>> parametersIterator = parameters.entrySet().iterator();
				Map.Entry<String, Object> entry;
				
				while (parametersIterator.hasNext()) {
					entry = parametersIterator.next();
					query.setParameter((String)entry.getKey(), entry.getValue());
				}
			}
			
			entity = query.getSingleResult();
			
			session.flush();
			transaction.commit();
		} catch (JDBCException jde) {
//			getLogger().severe(jde);
			transaction.rollback();
			throw new RuntimeException(jde);
		} catch (NoResultException nre) {
//			getLogger().info(nre.getMessage());
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		
		return entity;
	}
	
	public static long getNewId() {
		return NumberUtil.generateRandomStringId();
	}
	
//	protected MyLogger getLogger() {
//		if (this.logger == null) {
//			this.logger = new MyLogger(this.getClass());
//		}
//		
//		return this.logger;
//	}

}
