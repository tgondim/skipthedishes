package ca.skipthedishes.api.persistence.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.skipthedishes.api.model.entity.Customer;

public class CustomerSQLRepository extends SqlRepository<Customer> {

//	@Override
	public List<Customer> getAllObjects(int firstRow, int records) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("firstRow", firstRow);
		parameters.put("records", records);
		
		return executeTransactionNamedQueryResultList("Customer.getAllObjects", Customer.class, parameters);
	}

//	@Override
	public Customer getObject(long id) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", id);
	    
		return executeTransactionNamedQuerySingleResult("Customer.getObject", Customer.class, parameters);
	}

//	@Override
	public boolean deleteObject(long id) {
		Customer paymentMethodToDelete = getObject(id);
		
		if (paymentMethodToDelete == null) {
			return false;
		}
		
		return deleteObject(paymentMethodToDelete);
	}
}
