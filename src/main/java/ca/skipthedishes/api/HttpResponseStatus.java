package ca.skipthedishes.api;

public class HttpResponseStatus {

	public static int SUCCESS = 200;
	public static int HTTP_BAD_REQUEST = 400;
	public static int HTTP_NOT_AUTHORIZED = 401;

}
