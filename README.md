# README #

### ATTENTION!!! ###
THE API IS FUNCTIONAL AND WORKING (with a dummy repo).
Because of the short time we had to implement the whole solution, I used the strategy of implementing 
a dummy repository (in memory). After the API was completely functional, I implemented the real repository 
with hibernate/mysql.
Unfortunately I didn't have enough time to inject the repository into the models because the hackathon time was over.

## description
This is an Rest API with the followin features:
● Allow Authentication;
● Query Products;
● Receive Orders;
● Cancel an Order;
● Get Order Status;
● Store data in a database of his/her choice;

I used:
- Java 8
- Sparkjava framework (very fast and lightweight) for the HTTP request Lifecycle;
- Hibernate for the repository;
- JWT for the authentication;
- mysql for database;

## before Run you need to:
- execute the SQL script: initial_script.sql

## extras
There is a Postman 2.1 collection file with with data to test for all the API endpoints implemented on this solution
SKIPTheDishes API.postman_collection.json


